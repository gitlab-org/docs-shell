# Installation details

When you run `docs install`, the first thing Docs Shell will ask you is if you already have all repos cloned onto your computer:

```shell
Do you have all repos already cloned (GitLab [or GDK], Omnibus, Runner, Charts, Docs) (y/n)?
```

If you have all repos already, type "y" and press enter to set up your
existing folders as [paths](#paths) to the projects GitLab pulls from.
Otherwise, see [clone](#clone).

## Paths

Docs Shell will then prompt you to inform the paths to each repo, starting from:

```shell
Is /Users/username/path/to/docs-shell the path to your GitLab Shell repo (y/n)?
```

If you have the default file structure, you don't have to add each path;
type "y" and press enter and Docs Shell will assume the default structure.
Otherwise, type "n" and press enter for every repo you have in a different
directory and you'll be prompted to add its custom path.

For example, suppose you have the GitLab repo somewhere and it isn't GDK. When Docs Shell asks you:

```shell
Is /Users/username/path/to/gitlab the path to your GitLab repo (y/n)?
```

Type "n" and press enter. You'll be asked:

```shell
Is it GDK (y/n)?
```

Type "n" and press enter.

```shell
Enter the path to your GitLab repo:
```

Add the path to your GitLab repo and press enter:

```shell
/your/path/to/gitlab
```

Docs Shell will ask you the same thing for each repo.

TIP: to find the full path to a given folder, open it in a terminal and run
`pwd` and your shell will give you its path. Then you can copy it and paste it
wherever you want. Or, even easier, open Finder and the terminal side-by-side
and drag the folder from Finder to the terminal.

Illustration:

![Docs Shell set up with custom paths](img/docs_shell_custom_paths.mp4)

### GDK

If you use GitLab Development Kit, you can add it as custom path and work directly there. When Docs Shell asks:

```shell
Is /Users/username/path/to/gitlab the path to your GitLab repo (y/n)?
```

Type "n" and press enter. Shell will prompt you:

```shell
Is it GDK (y/n)?
```

Type "y" and press enter again. Then:

```shell
Enter the path to your GDK repo:
```

And add the full path to your repo and press enter:

```shell
/your/path/to/gdk
```

Docs Shell will consider your `gdk/gilab/doc` folder to build GitLab Docs on
the docs site, so you can edit docs directly there while you work on GDK. It
will watch docs live preview on port `5000` instead of conflicting with GDK's
`3000`.

Also, once GDK is set, you can run `docs gdk-update` to update GDK with less
hassle than the regular `gdk update` process. For further GDK commands you can run with Docs Shell, see [GDK](gdk.md).

### Reset paths

If you need to change the paths, you can run `docs install` again, or simply `docs update-paths`.

## Clone

Supported for GitLab team members only.

If you're starting fresh, Docs Shell can clone all repositories for you.

Make sure to clone this project into the folder where you want to clone all
the other repos needed into.

Docs Shell will ask you whether you're a GitLab Team member to choose the
correct namespace. If you are a team member, Docs Shell will assume
`gitlab-org` as namespace for all repos.<!--  Otherwise, it will ask you what's the namespace you forked your
projects into. -->

If you aren't a GitLab team member, fork and clone all repos before
installing Docs Shell.
<!-- Make sure you've forked all of them into the same namespace.  -->

Once done, run `docs install` to begin the installation of Docs Shell.

When the installation process begins, you'll be prompted:

```shell
Do you have all repos (GitLab [or GDK], Omnibus, Runner, Charts, Docs) cloned already (y/n)?
```

Type "y" and press "enter", then add the path to each repo when prompted.

Illustration:

![Docs Shell set up by cloning repos](img/docs_shell_clone.mp4)

<!-- 
Type "n" and press enter to start the cloning process. Docs Shell will try to
find an SSH connection to GitLab.com via `ssh -T git@gitlab.com`. If found,
you'll be led to clone through SSH. Otherwise, you'll be prompted to clone
through HTTPS.

If you have the projects forked into different namespaces, manually clone them
into the same directory on your computer then run `docs install`. Answer "y"
when Docs Shell asks "Do you have all repos (GitLab [or GDK], Omnibus, Runner,
Charts, Docs) cloned already (y/n)?", and from there you can set your
[custom paths](#paths). -->

## Compilation

Once the paths are set, Docs Shell will compile the site and open a browser
tab or window on the docs site. Give it a few seconds and it will display the
docs site locally.

## Troubleshooting

If you see any errors during the installation process, do not worry, let it finish.

If you tried to clone all repos with Docs Shell, look for them in Finder, they might have been installed in the wrong folder. If so:

- Drag and drop them all of them into the same folder in your computer.
- Open a terminal window in this higher-level folder and cd to `docs-shell`.
- Run `docs install` again. When prompted whether you have all repos cloned, type `y` and press enter.
- Choose the default file tree and let the installation process finish.

## Dependencies

The following depedencies are checked/installed with Docs Shell:

- ZSH
- (optional) oh-my-zsh
- (optional) iTerm2
- (required for macOS) xcode-select
- Ruby
  - Docs Ruby version
  - Local Ruby version
  - GitLab's Ruby version
- rbenv or rvm
- Git
- Homebrew+
- Rubygems
- Bundler
  - Docs Bundler version
  - GitLab Bundler version
- asdf
- NodeJS
- OpenSSL
- Nanoc+
- Yarn+
- markdownlint
- Vale
- haml-lint+
- yamllint
- scss-lint
- rspec

They're all checked during the installation process but you can run `docs dep`
any time to double-check them, as well as `docs dep --update` to update all of
them.

Illustration:

![Docs Shell checking dependencies](img/docs_dep.mp4)

