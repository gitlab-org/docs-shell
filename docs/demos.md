# Docs Shell demos

Watch the videos below to have a glance at Docs Shell working.

## Install Docs Shell

### Cloning all repos

![Install Docs Shell cloning all repos](img/install_clone.mp4)

### Repos already cloned

![Install Docs Shell custom paths](img/docs_shell_custom_paths.mp4)

## Reinstall Docs Shell

![Reinstall Docs Shell](img/docs_reinstall.mp4)

## Linting

Lint GitLab/Vale (`docs lint -c gitlab vale`) and
lint GitLab CI (`docs lint -d ci`):

![Linters](img/docs_lint.mp4)

## Dependency check

![Check dependencies](img/docs_dep.mp4)

## Update GDK

![Update GDK with Docs Shell](img/docs_gdk_update.mp4)

