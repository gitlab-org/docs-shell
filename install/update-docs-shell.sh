# Script for ZSH

# Test files (docs test files)
test_update_docs_shell() {
  echo 'install/update-docs-shell.sh ok'
}

# Test function
hello() {
  printf "Hello $USER! $WAVE \n"
}

# ---------------------------------------------------------------- #

# Update Docs Shell

# Check if upstream is set and up-to-date
check_upstream() {
  if (git remote | grep 'upstream' >/dev/null ); then
    if (git remote -v | grep 'marcia' >/dev/null ); then
      echo `tput setaf 1`"Upstream is outdated."`tput sgr0`
      echo `tput setaf 5`"Setting remote upstream to git@gitlab.com:gitlab-org/docs-shell.git..."`tput sgr0`
      echo `tput bold`'$ git remote remove upstream'`tput sgr0`
      git remote remove upstream
      echo `tput bold`'$ git remote add upstream git@gitlab.com:gitlab-org/docs-shell.git'`tput sgr0`
      git remote add upstream git@gitlab.com:gitlab-org/docs-shell.git
    else
      if (git remote show upstream | grep 'git@gitlab.com:gitlab-org/docs-shell.git' >/dev/null ); then
        echo `tput setaf 2`"Upstream is set and up-to-date."`tput sgr0`
      else
        echo "There's something wrong. Check your upstream repo:"
        git remote show upstream
      fi
    fi
  else
    echo `tput setaf 5`"Setting remote upstream to git@gitlab.com:gitlab-org/docs-shell.git..."`tput sgr0`
    echo `tput bold`'$ git remote add upstream git@gitlab.com:gitlab-org/docs-shell.git'`tput sgr0`
    git remote add upstream git@gitlab.com:gitlab-org/docs-shell.git
  fi
}

# Update local copy with remote upstream
update_upstream(){
  # Update upstream repo to gitlab-org's
  check_upstream
  # Check the default branch
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  echo "The default branch is `tput bold``tput setaf 2`$DEF_BRANCH`tput sgr0`."
  # Pull lastest changes from Docs Shell
  git_checkout_master
  echo "`tput bold`$ git pull upstream $DEF_BRANCH`tput sgr0`" ; git pull upstream $DEF_BRANCH
}

# Update local copy with remote origin
update-origin() {
  if (git remote show origin | grep 'git@gitlab.com:gitlab-org/docs-shell.git' >/dev/null ); then
    echo `tput setaf 2`"Origin is set and up-to-date."`tput sgr0`
  else
    echo `tput setaf 1`"Origin is not set."`tput sgr0`
    read "REPLY?`tput setaf 3`Are you a GitLab team member (y/n)?`tput sgr0`"`ok_sound`
      if [[ $REPLY =~ ^[Yy]$ ]] ; then
        if ssh-gitlab ; then
          echo `tput setaf 5`"Setting origin to git@gitlab.com:gitlab-org/docs-shell.git..."`tput sgr0`
          git remote remove origin>/dev/null
          echo `tput bold`'$ git remote add origin git@gitlab.com:gitlab-org/docs-shell.git'`tput sgr0`
          git remote add origin git@gitlab.com:gitlab-org/docs-shell.git
          ifsuccess
        else
          echo "SSH authentication to GitLab is not set."
          read "REPLY?`tput setaf 3`(Recommended) Would you like to set it up now (y/n)?`tput sgr0`"`ok_sound`
            if [[ $REPLY =~ ^[Yy]$ ]] ; then
              ssh_ask
              ssh_origin_docs_shell
            else
              echo `tput setaf 5`"Setting origin to https://gitlab.com/gitlab-org/docs-shell.git..."`tput sgr0`
              git remote remove origin>/dev/null
              echo `tput bold`'$ git remote add origin https://gitlab.com/gitlab-org/docs-shell.git'`tput sgr0`
              git remote add origin https://gitlab.com/gitlab-org/docs-shell.git
              ifsuccess
            fi
        fi
      else
        echo "Add the path to your forked repository:"
        read FORK_PATH
        FORKP=$FORK_PATH
        echo `tput setaf 5`"Setting origin to $FORKP..."`tput sgr0`
        git remote remove origin>/dev/null
        echo `tput bold`"$ git remote add origin $FORKP"`tput sgr0`
        git remote add origin $FORKP
        ifsuccess
      fi
  fi
}

# Update Docs Shell
shell() {
  go_shell
  # Check the default branch
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  echo "The default branch is `tput bold``tput setaf 2`$DEF_BRANCH`tput sgr0`."
  if [[ $1 == '--update' ]]; then
    check_upstream
    git_pull_check

    if [[ $2 == '--fork' ]]; then
      git_checkout_master
      echo `tput bold` '$ git fetch upstream' `tput sgr0` ; git pull upstream
      echo "`tput bold`$ git pull upstream $DEF_BRANCH" `tput sgr0` ; git pull origin $DEF_BRANCH
      echo "`tput bold`$ git push origin $DEF_BRANCH`tput sgr0`" ; git push origin $DEF_BRANCH
    fi
    pull
    dep
    reconfigure
  fi
}

# Re-source files
reconfigure(){
  . ./config.sh
  . ./functions.sh
  # Source config
    source config.sh
  # Source functions
    source functions.sh
  # Source user variables
  if [ -e functions/assets/user-variables.sh ] ; then
    source functions/assets/user-variables.sh
  fi

  # Source temp variables
  if [ -e functions/assets/temp-variables.sh ] ; then
    source functions/assets/temp-variables.sh
  fi

  # Source default paths
  if [ -e functions/assets/default-paths.sh ] ; then
    source functions/assets/default-paths.sh
  fi

  # Remove custom paths
  if [ -e functions/assets/custom-paths.sh ] ; then
    source functions/assets/custom-paths.sh
  fi

  # Remove fixed variables
  if [ -e functions/assets/custom-paths.sh ] ; then
    source functions/assets/fixed-variables.sh
  fi
}

# ---------------------------------------------------------------- #

# Remove user variables
remove_variables(){
  # Remove user variables (preview ports and GDK)
  if [ -e functions/assets/user-variables.sh ] ; then
    rm functions/assets/user-variables.sh
  fi

  # Remove temp variables (app variables)
  if [ -e functions/assets/temp-variables.sh ] ; then
    rm functions/assets/temp-variables.sh
  fi

  # Remove default paths if they exist
  if [ -e functions/assets/default-paths.sh ] ; then
    rm functions/assets/default-paths.sh
  fi

  # Remove custom paths if they exist
  if [ -e functions/assets/custom-paths.sh ] ; then
    rm functions/assets/custom-paths.sh
  fi

  # Remove fixed variables if they exist (SSH keys)
  if [ -e functions/assets/custom-paths.sh ] ; then
    rm functions/assets/fixed-variables.sh
  fi

  # Remove install log
  if [ -e .log ] ; then
    rm .log
  fi

  # Remove Gemfiles
  if [ -e Gemfile ] ; then
    rm Gemfile
  fi

  if [ -e Gemfile.lock ] ; then
    rm Gemfile.lock
  fi

  # Remove .paths (older version, deprecated) if they exist
  if [ -e .paths ] ; then
    rm .paths
  fi
}

# Clear path variables
clear_paths() {
  unset LOCAL
  unset GL
  unset DSHELL
  unset DOC
  unset GITLAB
  unset OMN
  unset RUN
  unset CHAR
}

# ---------------------------------------------------------------- #

# Reinstall Docs Shell (necessary upgrade to version 1.0.5 or higher)

# Reinstall
reinstall(){
  CURRENT=$(pwd)
  tput setaf 5 ; echo "Reinstalling Docs Shell..." ; tput sgr0
  ok_sound ; read "REPLY?Is `tput sgr0`$CURRENT`tput setaf 3` the path to your Docs Shell directory to reinstall from (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    # Clear current path variables
    clear_paths
    # Update local copy with upstream repo
    update_upstream
    # Reinstall
    reinstall_finish
  else
    echo "Enter the path to the Docs Shell repo you want to install from:"
    read NEW_DSHELL_REPO
    NEW_DSHELL=$NEW_DSHELL_REPO
    cd $NEW_DSHELL
    # Clear current path variables
    clear_paths
    # Set new CURRENT
    export CURRENT=$(pwd)
    # Update local copy with upstream repo
    update_upstream
    # Reinstall
    reinstall_finish
  fi
}

# Reinstall and update dependencies
reinstall_finish(){
  # Update paths
  chmod +x $CURRENT/docs.sh
  export PATH=$PATH:$CURRENT
  # Update permalias
  perm_alias
  # Remove user variables
  remove_variables
  # Clone
  if [[ $1 == "--clone" ]] ; then
    clone_temp
    export CLONE='0'
  else
    # Reset paths
    echo `tput setaf 5`"Resetting your paths..."`tput sgr0`
    paths
  fi
  # Install GDK
  if [[ $DGDK == '0' ]] ; then
    # Set GDK Ruby
    gdk_ruby
  else
    # Ask if they want to set GDK
    ask_install_gdk
  fi
  # Source new files
  reconfigure
  # Dependencies
  echo `tput setaf 5`"Dependencies check:"`tput sgr0`
  check_dependencies
  # Installation log file
  echo "Installed on ${NOW}" > $DSHELL/.log
  # Pull all repos
  echo `tput setaf 5`"Pulling all repos..."`tput sgr0`
  pull
  # Compile
  echo `tput setaf 5`"Compiling GitLab Docs..."`tput sgr0`
  compile
  if [[ $1 == '--start' ]]; then
    cd $DOC
    echo `tput setaf 5`"Starting live preview..." `tput sgr0`
    echo `tput bold`"$ bundle exec nanoc live -p $PORT" `tput sgr0`; open_preview | bundle exec nanoc live -p $PORT
  fi
}
