# Test file (docs test files)
test_update_dep() {
  echo 'install/update-dep.sh ok'
}

# ---------------------------------------------------------------- #

# Update dependencies

update_dep() {
  ssh-gitlab
  latest_docs
  tput setaf 5 ; echo "Updating dependencies and package managers. This may take a while.." ; tput sgr0
  tput setaf 5 ; echo "Updating XCode..." ; tput sgr0
  echo `tput bold` '$ xcode-select --install' `tput sgr0` ; xcode-select --install
  rvm_update_dep
  tput setaf 5 ; echo "Updating Ruby..." ; tput sgr0
  compare_rubies
  tput setaf 5 ; echo "Updating RubyGems..." ; tput sgr0
  echo `tput bold` '$ gem update --system --no-document --quiet' `tput sgr0` ; gem update --system --no-document
  check_bundler
  nanoc_update_dep
  dep_cleanup
  tput setaf 5 ; echo "Updating npm..." ; tput sgr0
  echo `tput bold` '$ npm install -g npm' `tput sgr0` ; npm install -g npm
  echo `tput setaf 5`"Updating asdf..." `tput sgr0`
  echo `tput bold` '$ asdf update' `tput sgr0` ; asdf update
  check_asdf
  tput setaf 5 ; echo "Updating markdownlint..." ; tput sgr0
  echo `tput bold` '$ npm install -g markdownlint-cli' `tput sgr0` ; npm install -g markdownlint-cli
  tput setaf 5 ; echo "Updating Homebrew..." ; tput sgr0
  echo `tput bold` '$ brew update' `tput sgr0` ; brew update
  tput setaf 5 ; echo "Updating dependencies previously installed with Homebrew..." ; tput sgr0
  echo `tput bold`'$ brew upgrade'`tput sgr0` ; brew upgrade
  oh-my-zsh_update_dep
  yarn_update_dep
  tput setaf 5 ; echo "Check your system for potential problems..." ; tput sgr0
  echo `tput bold` '$ brew doctor' `tput sgr0` ; brew doctor
  software_update
  tput bold ; tput setaf 2; printf "${TADA} Dependencies updated. `tput sgr0` (${NOW}) \n"
}

rvm_update_dep() {
  if [[ $RVMANAGER == 'rvm' ]] ; then
    tput setaf 5 ; echo "Updating RVM..." ; tput sgr0
    echo `tput bold` '$ rvm get stable' `tput sgr0` ; rvm get stable
  fi
}

dep_cleanup() {
  tput setaf 5 ; echo "Cleaning up gems..." ; tput sgr0
  echo `tput bold` '$ gem cleanup' `tput sgr0` ; gem cleanup
  tput setaf 5 ; echo "Cleaning up Homebrew..." ; tput sgr0
  echo `tput bold` '$ brew cleanup -n' `tput sgr0` ; brew cleanup -n
}

compare_rubies() {
  cd $DOC
  check_compare_rubies
  cd $DSHELL
}

nanoc_update_dep() {
  cd $DOC
  echo `tput setaf 5`"Updating Nanoc..." `tput sgr0`
  echo `tput bold` '$ gem update nanoc' `tput sgr0` ; gem update nanoc >/dev/null
  tput setaf 5 ; echo "Updating Nanoc dependencies..." ; tput sgr0
  echo `tput bold` "$ bundle "_"$DBUN"_" install --quiet" `tput sgr0` ; bundle '_'$DBUN'_' install --quiet
  cd $DSHELL
}

yarn_update_dep() {
  cd $DOC
  echo `tput setaf 5`"Updating Yarn dependencies..." `tput sgr0`
  echo `tput bold` '$ yarn upgrade --silent' `tput sgr0` ; yarn upgrade --silent
  cd $DSHELL
}

oh-my-zsh_update_dep() {
  if [ -d ~/.oh-my-zsh ]
  then
    tput setaf 5 ; echo "Updating oh-my-zsh..." ; tput sgr0
    echo `tput bold` '$ omz update' `tput sgr0` ; omz update
  fi
}

software_update() {
  tput setaf 5 ; echo "Checking your system for further updates..." ; tput sgr0
  echo `tput bold` '$ softwareupdate --list' `tput sgr0` ; softwareupdate --list
}
