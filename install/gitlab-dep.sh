# Test file (docs test files)
test_install_gl_dep() {
  echo 'install/gitlab-dep.sh ok'
}

# ---------------------------------------------------------------- #

# Compare GitLab Docs and GitLab Rubies

# Compare GitLab's and Docs' rubies
gitlab_ruby_compare() {
  cd $GITLAB
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  cd $DSHELL
  echo `tput setaf 5`"Checking GitLab's Ruby version..." `tput sgr0`
  GLRUBY=$(curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/$DEF_BRANCH/.ruby-version > .gitlab-ruby && cat .gitlab-ruby)
  if (grep 'GLRUBY' functions/assets/temp-variables.sh >/dev/null) ; then
    sed -i '' '/GLRUBY/d' functions/assets/temp-variables.sh
  fi
  echo "GLRUBY='$GLRUBY'" >> functions/assets/temp-variables.sh
  export GLRUBY="$GLRUBY"
  source functions/assets/temp-variables.sh
  echo "GitLab's current Ruby is $GLRUBY."
  echo `tput setaf 5`"Checking GitLab Docs' Ruby version..." `tput sgr0`
  gldocs_ruby
  echo "GitLab Docs' current Ruby is $DOCSRUBY."
  if [[ $GLRUBY != $DOCSRUBY ]]; then
    echo `tput setaf 1`"Houston, we have a problem.`tput sgr0` GitLab is using Ruby $GLRUBY and GitLab Docs, Ruby $DOCSRUBY. Please ask someone from the GitLab Team to update."
    echo "Swithing to GitLab's Ruby version..."
    if [[ $RVMANAGER == 'rvm' ]] ; then
      echo `tput bold`"$ rvm use $GLRUBY"`tput sgr0` ; rvm use $GLRUBY
    else [[ $RVMANAGER == 'rbenv' ]]
      echo `tput bold`"$ rbenv shell $GLRUBY"`tput sgr0` ; rbenv shell $GLRUBY
    fi
  else
    tput setaf 2 ; echo "Rubies match." ; tput sgr0
  fi
  rm .gitlab-ruby
}

# Compare GitLab Docs and GitLab Bundler

gitlab_bundler_compare() {
  cd $GITLAB
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  cd $DSHELL
  echo `tput setaf 5`"Checking GitLab's Bundler version..." `tput sgr0`
  curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/$DEF_BRANCH/Gemfile.lock > .gitlab-lock
  GLBUN=$(awk '/BUNDLED WITH/{getline; $1=$1 ; print}' .gitlab-lock)
  if (grep 'GLBUN' functions/assets/temp-variables.sh >/dev/null) ; then
    sed -i '' '/GLBUN/d' functions/assets/temp-variables.sh
  fi
  echo "GLBUN='$GLBUN'" >> functions/assets/temp-variables.sh
  export GLBUN="$GLBUN"
  source functions/assets/temp-variables.sh
  echo "GitLab's current Bundler version is $GLBUN."
  echo `tput setaf 5`"Checking GitLab Docs' Bundler version..." `tput sgr0`
  docs_bundler
  echo "GitLab Docs' current Bundler version is $DBUN."
  if [[ $GLBUN != $DBUN ]]; then
    echo `tput setaf 1`"Houston, we have a problem.`tput sgr0` GitLab is using Bundler `tput bold`$GLBUN`tput sgr0` and GitLab Docs, Bundler `tput bold`$DBUN`tput sgr0`. Please ask someone from the Team to update."
    echo `tput setaf 5`"Installing GitLab's Bundler version..."`tput sgr0`
    echo `tput bold` "$ gem install bundler:$GLBUN" `tput sgr0` ; gem install bundler:$GLBUN
  else
    tput setaf 2 ; echo "Bundlers match." ; tput sgr0
  fi
  rm .gitlab-lock
}

# ---------------------------------------------------------------- #

# Generate Gemfile
gemfile() {
  if [[ -e Gemfile ]]; then
    sed -i '' "s%ruby .*%ruby \"$GLRUBY\"%g" Gemfile
  else
    printf "source \"https://rubygems.org\"\nruby \"$GLRUBY\"\n" > Gemfile
  fi
}

# ---------------------------------------------------------------- #

# haml_lint-only dependencies
## content linter for GitLab repo only (checks app/views links and anchors from the UI to /help)

haml_lint_gems(){
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  # Curl up-to-date Gemfile from GitLab
  curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/$DEF_BRANCH/Gemfile > .gitlab-gemfile
  # Generate temp Gemfile
  echo "source 'https://rubygems.org'" > .haml-gemfile
  grep -w "gem 'rails'" .gitlab-gemfile >> .haml-gemfile
  grep -w "gem 'haml_lint'" .gitlab-gemfile >> .haml-gemfile
  rm .gitlab-gemfile
}

# All GL dependencies

gitlab_all_gems(){
    git_pull_check
    DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
    # If $GITLAB isn't up-to-date, curl GitLab's up-to-date Gemfile to a temp file
    echo `tput setaf 5`"Bundling from up-to-date Gemfile..." `tput sgr0`
    curl -s https://gitlab.com/gitlab-org/gitlab/-/raw/$DEF_BRANCH/Gemfile > .gitlab-gemfile
    # Bundle GitLab with up-to-date temp Gemfile
    echo `tput bold`"$ bundle "_"$GLBUN"_" install"`tput sgr0`
    BUNDLE_GEMFILE=.gitlab-gemfile bundle "_"$GLBUN"_" install
    # Remove temp files
    if [[ -e .gitlab-gemfile ]]; then
      rm .gitlab-gemfile
    fi
    if [[ -e .gitlab-gemfile.lock ]]; then
      rm .gitlab-gemfile.lock
    fi
}

# ---------------------------------------------------------------- #

# Check GitLab dependencies

gl_dep() {
  echo `tput setaf 5`"Checking GitLab's dependencies:" `tput sgr0`
  # Compare GitLab and GL Docs rubies
  gitlab_ruby_compare
  # Remove current Gemfiles.lock before updating
  if [[ -e Gemfile ]]; then
    rm Gemfile
  fi
  if [[ -e Gemfile.lock ]]; then
    rm Gemfile.lock
  fi
  # Compare GitLab and GL Docs Bundler
  gitlab_bundler_compare
  # Source new variables
  source functions/assets/temp-variables.sh
  source functions/assets/user-variables.sh
  if [[ $DGDK == '0' ]]; then
    # If GDK is set, install all GL dependencies
    echo `tput setaf 5`"Bundling all GitLab gems. This might take a while..."`tput sgr0`
    # Pull GDK first
    cd $GDK_SEP
    echo `tput setaf 5`"Pulling GDK..."`tput sgr0`
    git_pull_check
    # For $GITLAB haml-lint:
    cd $GITLAB
    echo `tput setaf 5`"Pulling GitLab..."`tput sgr0`
    gitlab_all_gems
    # If there's a separate GitLab (within GDK):
    # Repeat for $GDK_SEP_GL haml-lint:
    if $GDK_SEP_GL; then
      cd $GDK_SEP_GL
      echo `tput setaf 5`"Pulling GitLab (within GDK)..."`tput sgr0`
      gitlab_all_gems
    fi
    # Back to Docs Shell:
    cd $DSHELL
  else
    # If GDK is not set, install only haml-lint dependencies
    echo `tput setaf 5`"Installing GitLab's dependencies required for haml-lint..."`tput sgr0`
    cd $GITLAB
    haml_lint_gems
    echo `tput bold`"$ bundle "_"$GLBUN"_" install"`tput sgr0`
    # Bundle from temp Gemfile
    BUNDLE_GEMFILE=.haml-gemfile bundle "_"$GLBUN"_" install
    # Remove temp files
    if [[ -e .haml-gemfile ]]; then
      rm .haml-gemfile
    fi
    if [[ -e .haml-gemfile.lock ]]; then
      rm .haml-gemfile.lock
    fi
    cd $DSHELL
  fi
  if [[ $GLRUBY != $DOCSRUBY ]]; then
    echo "Swithing back to GitLab Docs' Ruby version..."
    if [[ $RVMANAGER == 'rvm' ]] ; then
      echo `tput bold`"$ rvm use $DOCSRUBY"`tput sgr0` ; rvm use $DOCSRUBY
    else [[ $RVMANAGER == 'rbenv' ]]
      echo `tput bold`"$ rbenv shell $DOCSRUBY"`tput sgr0` ; rbenv shell $DOCSRUBY
    fi
  fi
  # Remove temp .tool-versions
  cd $DSHELL
  if [ -e .tool-versions ] ; then
     rm .tool-versions
  fi
  # Remove temp Gemfiles
  if [[ -e Gemfile ]]; then
    rm Gemfile
  fi
  if [[ -e Gemfile.lock ]]; then
    rm Gemfile.lock
  fi
}
