# Script for ZSH

# Test file (docs test files)
test_install_clone_temp() {
  echo 'install/clone_temp.sh ok'
}

# ---------------------------------------------------------------- #

# Clone all repositories
# Temporary solution for GL team members until complete refactor of the clone/paths functions

clone_temp(){
  # Define dir to clone repos into
  export LOCAL=$(_loc)
  export GL="$LOCAL/"
  # Remove current default paths if they exist
  if [ -e functions/assets/default-paths.sh ] ; then
    rm functions/assets/default-paths.sh
  fi
  # Remove custom paths if they exist
  if [ -e functions/assets/custom-paths.sh ] ; then
    rm functions/assets/custom-paths.sh
  fi
  # Define new default paths
  echo "LOCAL='$LOCAL'" > functions/assets/default-paths.sh
  echo "GL='$GL'" >> functions/assets/default-paths.sh
  echo "DSHELL='$LOCAL/docs-shell'" >> functions/assets/default-paths.sh
  export DSHELL="$DSHELL"
  source functions/assets/default-paths.sh
  # Check SSH connection to GitLab
  if ! ssh_check; then
    echo "SSH authentication to GitLab is required."
    ok_sound
    read "REPLY?`tput setaf 3`Would you like to set it up now (y/n)?`tput sgr0`"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      ssh_ask
      clone_repos_ssh_new
      ssh_origin_docs_shell
    else
      error_sound
      tput setaf 1 ; echo "We cannot clone through SSH. Aborting." ; tput sgr0
    fi
  else
    clone_repos_ssh_new
    ssh_origin_docs_shell
  fi
}

clone_repos_ssh_new(){
  # Clone repos
  echo `tput setaf 5`"Cloning repositories..."`tput sgr0`
  ok_sound ; tput setaf 3
  read "REPLY?All repos will be cloned into "`tput bold`"$GL"`tput sgr0 ; tput setaf 3`". This will take some time. Continue (y/n)?`tput sgr0`"`ok_sound`
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    for k in "${(@k)repos_array}" ; do
      echo `tput setaf 5`"Cloning $repos_array[$k]..."`tput sgr0`
      cd $GL
      if [[ $k == 'charts/gitlab' ]]; then
        echo `tput bold` "$ git clone git@gitlab.com:gitlab-org/$k.git charts" `tput sgr0`
        git clone git@gitlab.com:gitlab-org/$k.git charts
        if [[ $? != 0 ]] ; then
          error_sound ; echo `tput setaf 1` "Failed to clone $repos_array[$k]." `tput sgr0`
        else
          tput setaf 2; echo "Cloned $repos_array[$k] into $PWD"; tput sgr0
          echo "The path to `tput bold`$repos_array[$k]`tput sgr0` is `tput bold`$GL$k"`tput sgr0`
        fi
      else
        echo `tput bold` "$ git clone git@gitlab.com:gitlab-org/$k.git" `tput sgr0`
        git clone git@gitlab.com:gitlab-org/$k.git
        if [[ $? != 0 ]] ; then
          error_sound ; echo `tput setaf 1` "Failed to clone $repos_array[$k]." `tput sgr0`
        else
          tput setaf 2; echo "Cloned $repos_array[$k] into $PWD"; tput sgr0
          echo "The path to `tput bold`$repos_array[$k]`tput sgr0` is `tput bold`$GL$k"`tput sgr0`
        fi
      fi
      # Define paths
      case $k in
        gitlab)
          export GITLAB=$GL'gitlab'
          echo "GITLAB='$GITLAB'" >> $DSHELL/functions/assets/default-paths.sh
        ;;
        gitlab-docs)
          export DOC=$GL'gitlab-docs'
          echo "DOC='$DOC'" >> $DSHELL/functions/assets/default-paths.sh
        ;;
        charts/gitlab)
          export CHAR=$GL'charts'
          echo "CHAR='$CHAR'" >> $DSHELL/functions/assets/default-paths.sh
        ;;
        gitlab-runner)
          export RUN=$GL'gitlab-runner'
          echo "RUN='$RUN'" >> $DSHELL/functions/assets/default-paths.sh
        ;;
        omnibus-gitlab)
          export OMN=$GL'omnibus-gitlab'
          echo "OMN='$OMN'" >> $DSHELL/functions/assets/default-paths.sh
        ;;
      esac
    done
    source $DSHELL/functions/assets/default-paths.sh
    cd $DSHELL
    # Set GitLab Docs port to 3000
    # Remove port if exists
    if [ -e functions/assets/user-variables.sh ] ; then
      if (grep 'PORT' functions/assets/user-variables.sh >/dev/null) ; then
        sed -i '' '/PORT/d' functions/assets/user-variables.sh
      fi
    fi
    export PORT="3000"
    echo "PORT='3000'" >> $DSHELL/functions/assets/user-variables.sh
    tput setaf 2 ; echo "Preview GitLab Docs on port 3000." ; tput sgr0
    # Symlink content folders
    symlink_gitlab
    symlinks
    symlink_check_all
    DGDK='1'
    source $DSHELL/config.sh
    source $DSHELL/functions/assets/user-variables.sh
    source $DSHELL/functions/assets/variables.sh
  else
    echo "Ok. Aborting."
    return
  fi
}
