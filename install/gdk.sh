# THIS IS A WORK IN PROGRESS

# Test files (docs test files)
test_gdk() {
  echo 'functions/install/gdk.sh ok'
}

# Set up GDK on macOS

# ---------------------------------------------------------------- #

#### Set up GDK and GDK port
# GDK must have been cloned already

gdk-set() {
  gdk_set_path
  gdk_ruby
  gl_dep
}

# Set up a different GDK repo
gdk_set_path() {
  if [[ -e $DSHELL/functions/assets/temp-variables.sh ]]; then
    # Remove GDK_SEP variable
    if (grep 'GDK_SEP' $DSHELL/functions/assets/temp-variables.sh >/dev/null) ; then
      sed -i '' '/GDK_SEP/d' $DSHELL/functions/assets/temp-variables.sh
    fi
  fi

  # Read GDK repo
  ok_sound ; echo `tput setaf 3`"Enter the path to your GDK repo:"`tput sgr0`
  read GDK_SEP
  echo "GDK_SEP='$GDK_SEP'" >> $DSHELL/functions/assets/temp-variables.sh
  export GDK_SEP="$GDK_SEP"

  # Update GDK_SEP_GL variable
  if (grep 'GDK_SEP_GL' $DSHELL/functions/assets/temp-variables.sh >/dev/null) ; then
    sed -i '' '/GDK_SEP_GL/d' $DSHELL/functions/assets/temp-variables.sh
  fi
  echo "GDK_SEP_GL='$GDK_SEP/gitlab'" >> $DSHELL/functions/assets/temp-variables.sh
  export GDK_SEP_GL="$GDK_SEP_GL"

  # Reload variables
  source $DSHELL/functions/assets/temp-variables.sh

  # Set new port to preview GDK and Docs
  echo `tput setaf 5`"Setting GitLab Docs preview port to 5000..."`tput sgr0`
  if [[ -e $DSHELL/functions/assets/user-variables.sh ]]; then
    # Replace PORT variable
    if (grep 'PORT' $DSHELL/functions/assets/user-variables.sh >/dev/null) ; then
      sed -i '' '/PORT/d' $DSHELL/functions/assets/user-variables.sh
    fi
  fi
  echo "PORT='5000'" >> $DSHELL/functions/assets/user-variables.sh
  export PORT="5000"
  source $DSHELL/functions/assets/user-variables.sh

  # Set GDK port
  echo `tput setaf 5`"Setting GDK preview port to 3000..."`tput sgr0`
  # Replace GDK_PORT variable
  if (grep 'GDK_PORT' $DSHELL/functions/assets/user-variables.sh >/dev/null) ; then
    sed -i '' '/GDK_PORT/d' $DSHELL/functions/assets/user-variables.sh
  fi
  echo "GDK_PORT='3000'" >> $DSHELL/functions/assets/user-variables.sh
  export GDK_PORT="3000"
 
  # Set DGDK to zero (means "user uses GDK")
  if (grep 'DGDK' $DSHELL/functions/assets/user-variables.sh >/dev/null) ; then
    sed -i '' '/DGDK/d' $DSHELL/functions/assets/user-variables.sh
  fi
  echo "DGDK='0'" >> $DSHELL/functions/assets/user-variables.sh
  export DGDK="0"
 
  # Reload variables
  source $DSHELL/functions/assets/user-variables.sh
  tput setaf 2 ; echo "Preview GitLab Docs on port 5000 and GDK on port 3000." ; tput sgr0
}

# ---------------------------------------------------------------- #

# GDK Ruby version

# Update and compare GitLab's and Docs' rubies
gdk_ruby() {
  cd $GDK_SEP
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  cd $DSHELL
  GDKRUBY=$(curl -s https://gitlab.com/gitlab-org/gitlab-development-kit/-/raw/$DEF_BRANCH/.ruby-version > .gdk-ruby && cat .gdk-ruby)
  if (grep 'GDKRUBY' functions/assets/temp-variables.sh >/dev/null) ; then
    sed -i '' '/GDKRUBY/d' functions/assets/temp-variables.sh
  fi
  echo "GDKRUBY='$GDKRUBY'" >> functions/assets/temp-variables.sh
  export GDKRUBY="$GDKRUBY"
  source functions/assets/temp-variables.sh
  rm .gdk-ruby
}

# Switch to GDK' Ruby version
gdk_switch_ruby() {
  gdk_ruby
  if [[ $RVMANAGER == 'rvm' ]] ; then
    echo `tput bold`"$ rvm use $GDKRUBY"`tput sgr0` ; rvm use $GDKRUBY
  else [[ $RVMANAGER == 'rbenv' ]]
    echo `tput bold`"$ rbenv shell $GDKRUBY"`tput sgr0` ; rbenv shell $GDKRUBY
  fi
}

# ---------------------------------------------------------------- #

# GDK gem check
## Necessary for troubleshooting rvm/rbenv/asdf or when installing a new ruby

gdk_check_gem(){
  if ! gem list '^gitlab-development-kit$' -i >/dev/null ; then
    echo `tput setaf 5`"Installing GDK gem..."`tput sgr0`
    echo `tput bold`"$ gem install gitlab-development-kit"`tput sgr0` ; gem install gitlab-development-kit
    gem install gitlab-development-kit
  fi
}

# ---------------------------------------------------------------- #

# Update GDK
# `docs gdk-update`

# Flags:
# `docs gdk-update --min` for minimal update.
# `docs gdk-update --start` to start GDK after update.
# `docs gdk-update --min --start` for minimal update followed by `gdk start`.
# `docs gdk-update --reset` to reset the DB and skip reset question.
gdk-update() {
  if [[ $GDK_SEP ]]; then
    cd $GDK_SEP
    if [[ $1 == '--min' ]]; then
      gdk_pre_update
      gdk_post_update
      if [[ $2 == '--start' ]]; then
        echo `tput bold`"$ gdk start"`tput sgr0` ; gdk start
        open http://localhost:$GDK_PORT
      fi
    else
      gdk_pre_update
      gdk_reset_db_q
      gdk_post_update
      if [[ $1 == '--start' ]]; then
        echo `tput bold`"$ gdk start"`tput sgr0` ; gdk start
        open http://localhost:$GDK_PORT
      fi
    fi
  else
    echo "GDK is not set."
    ask_install_gdk
  fi
}

# ---------------------------------------------------------------- #

#### DB RESET

# Pass the flag --reset to skip reset question
gdk_reset_db_q() {
  if [[ $1 == '--reset' ]]; then
    gdk_backup_data
    echo `tput setaf 5`"Creating new data..."`tput sgr0`
    echo `tput bold`"$ make postgresql/data"`tput sgr0` ; make postgresql/data
  else
    gdk_reset_db
  fi
}

# Ask to reset DB
gdk_reset_db(){
  ok_sound ; read "REPLY?`tput setaf 3`Back up and reset database data (y/n)?"`tput sgr0`
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    gdk_backup_data
    echo `tput setaf 5`"Creating new data..."`tput sgr0`
    echo `tput bold`"$ make postgresql/data"`tput sgr0` ; make postgresql/data
  else
    echo "Ok, let's keep it as it is."
  fi
}

# Backup GDK data to reset
gdk_backup_data() {
  # Trick to update GDK when postgress is messed up
  cd $GDK_SEP
  date=$(date +"%Y-%m-%d_%Hh%Mmin%Ss")
  echo `tput setaf 5`"Backing up data..."`tput sgr0`
  mv postgresql/data postgresql/"data-bkp_${date}"
  echo `tput setaf 2`"Data backed up into "`tput sgr0; tput bold`"$GDK_SEP/postgresql/data-bkp_${date}"`tput sgr0`
}

# ---------------------------------------------------------------- #

#### REGULAR UPDATE

# Prepare for GDK update
gdk_pre_update (){
  asdf-config-legacy
  echo `tput setaf 5`"Switching to GDK's Ruby version..."`tput sgr0`
  #gdk_switch_ruby
  gdk_check_gem
  cd $GDK_SEP
  echo `tput setaf 5`"Stopping GDK..."`tput sgr0`
  echo `tput bold`"$ gdk stop"`tput sgr0`; gdk stop
  echo `tput setaf 5`"Fetching GDK repo..."`tput sgr0`
  git_pull_check
  echo `tput setaf 5`"Bundling GDK repo..."`tput sgr0`
  echo `tput bold`"$ bundle install"`tput sgr0`; bundle install
  cd $GDK_SEP_GL
  echo `tput setaf 5`"Fetching GitLab repo..."`tput sgr0`
  echo "I'm in $PWD"
  git_pull_check
  echo `tput setaf 5`"Bundling GitLab repo..."`tput sgr0`
  echo `tput bold`"$ bundle install"`tput sgr0`; bundle install
  cd $GDK_SEP
  echo `tput setaf 5`"Fetching GitLab Shell repo..."`tput sgr0`
  cd gitlab-shell
  echo "I'm in $PWD"
  git_pull_check
  echo `tput setaf 5`"Bundling GitLab Shell repo..."`tput sgr0`
  echo `tput bold`"$ bundle install"`tput sgr0`; bundle install
  cd $GDK_SEP
}

# Migrate DB
gdk_migrate_db() {
  cd $GDK_SEP_GL
  echo `tput setaf 5`"Migrating the database..."`tput sgr0`
  echo `tput bold`"$ bundle exec rails db:migrate RAILS_ENV=development"`tput sgr0`; bundle exec rails db:migrate RAILS_ENV=development
  cd $GDK_SEP
}

# Finally, update GDK
gdk_post_update() {
  echo `tput setaf 5`"Updating GDK..."`tput sgr0`
  echo `tput bold`"$ gdk update"`tput sgr0`; gdk update
  echo `tput setaf 5`"Make bootstrap..."`tput sgr0`
  echo `tput bold`"$ make bootstrap"`tput sgr0`; make bootstrap
  echo `tput setaf 5`"Reconfigure GDK..."`tput sgr0`
  echo `tput bold`"$ gdk reconfigure"`tput sgr0`; gdk reconfigure
  gdk_migrate_db
}

# ---------------------------------------------------------------- #

#### TROUBLESHOOTING UPDATE

# THIS IS A WORK IN PROGRESS

# Upgrade GDK troubleshooting

# `docs gdk-update-tbs`
# `docs gdk-update-tbs --deep` for extra tbs possible fixes.
# `docs gdk-update-tbs --reset` to reset the DB and skip reset question.
gdk-update-tbs() {
  gdk_pre_update
  gdk_update_gl_dep
  gdk_reset_db_q ## --reset
  gdk_update_gl_dep ## --deep
  gdk_bundle_gdk_repo ## deep
  gdk_post_update
}

# Force manual update of GL's dependencies (should update with `gdk update`)
gdk_update_gl_dep() {
  cd $GDK_SEP_GL
  echo `tput setaf 5`"Updating GitLab's dependencies..."`tput sgr0`
  echo `tput setaf 5`"Updating Homebrew and its dependencies..."`tput sgr0`
  echo `tput bold`"$ brew update && brew upgrade yarn asdf"`tput sgr0` ; brew update && brew upgrade yarn asdf
  echo `tput setaf 5`"Updating asdf and its dependencies..."`tput sgr0`
  echo `tput bold`"$ asdf plugin update --all"`tput sgr0`; asdf plugin update --all
  echo `tput setaf 5`"Bundling everything..."`tput sgr0`
  echo `tput bold`"$ asdf install"`tput sgr0`; asdf install
  echo `tput bold`"$ bundle install && yarn install"`tput sgr0`; bundle install && yarn install
  echo `tput bold`"$ bundle check"`tput sgr0`; bundle check
  echo `tput bold`"$ bundle pristine pg"`tput sgr0`; bundle pristine pg
  if [[ $1 == '--deep' ]]; then
    echo `tput bold`"$ brew unlink gnupg && brew link gnupg"`tput sgr0` ; brew unlink gnupg && brew link gnupg
    echo `tput setaf 5`"Restoring the installed gems into their pristine condition..."`tput sgr0`
    echo `tput bold`"$ gem pristine --all"`tput sgr0`; gem pristine --all
  fi
  cd $GDK_SEP
}

# Bundle GDK repo (should update with `gdk update`)
# Note: I'm not sure we need this at all :thinking:
gdk_bundle_gdk_repo() {
  echo `tput setaf 5`"Bundling dependencies..."`tput sgr0`
  echo `tput bold`"$ asdf plugin update --all"`tput sgr0`; asdf plugin update --all
  echo `tput bold`"$ asdf install"`tput sgr0`; asdf install
  echo `tput bold`"$ bundle install && yarn install"`tput sgr0`; bundle install && yarn install
  if [[ $1 == '--deep' ]]; then
    echo `tput setaf 5`"Restoring the installed gems into their pristine condition..."`tput sgr0`
    echo `tput bold`"$ gem pristine --all"`tput sgr0`; gem pristine --all
  fi
}

# ---------------------------------------------------------------- #

#### LINT GITLAB FROM GDK

# Frontend eslint, jest, and prettier
# Pass the flag `-w` for prettier --write
gdk-lint-fe() {
  cd $GDK_SEP_GL
  changes=`git diff --name-only`
  if [[ $changes ]]; then
    echo "I'm in $GDK_SEP_GL"
    # Vue files
      files_vue=`git diff --name-only | grep -E '.vue$' `
      for file_vue in $files_vue; do
        if [[ $file_vue ]]; then
          echo "`tput setaf 5`Checking .vue files:\n`tput sgr0`$file_vue"
        fi
        echo `tput bold`"$ yarn eslint `tput setaf 5`$file_vue"`tput sgr0`; yarn eslint $file_vue
        if [[ $1 == "-w" ]]; then
          echo `tput bold`"$ yarn prettier --write `tput setaf 5`$file_vue"`tput sgr0`; yarn prettier --write $file_vue
        else
          echo `tput bold`"$ yarn prettier --check `tput setaf 5`$file_vue"`tput sgr0`; yarn prettier --check $file_vue
        fi
      done
      # JS files
      files_js=`git diff --name-only | grep -E '.js$' `
      for file_js in $files_js; do
        if [[ $file_js ]]; then
          echo "`tput setaf 5`Checking .js files:\n`tput sgr0`$file_js"
        fi
        echo `tput bold`"$ yarn eslint `tput setaf 5`$file_js"`tput sgr0`; yarn eslint $file_js
        if [[ $1 == "-w" ]]; then
          echo `tput bold`"$ yarn prettier --write `tput setaf 5`$file_js"`tput sgr0`; yarn prettier --write $file_js
        else
          echo `tput bold`"$ yarn prettier --check `tput setaf 5`$file_js"`tput sgr0`; yarn prettier --check $file_js
        fi
      done
  else
    echo `tput setaf 6`"There are no .js or .vue files to lint."`tput sgr0`
  fi
}

gdk_lint_haml() {
  cd $GDK_SEP_GL
  echo "I'm in $PWD"
  # Generate translation file
  bin/rake gettext:regenerate
  # Lint
  scripts/static-analysis
}

gdk_gen_translation(){
  cd $GDK_SEP_GL
  echo "I'm in $PWD"
  # Generate translation file
  bin/rake gettext:regenerate
}

gdk_lint_static() {
  cd $GDK_SEP_GL
  echo "I'm in $PWD"
  scripts/static-analysis
}
