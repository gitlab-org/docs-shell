#!/bin/bash

# Paths #
source install/paths.sh

# Variables #
source functions/assets/variables.sh

# Functions #
source functions.sh

# User variables
if [ -e functions/assets/user-variables.sh ] ; then
  source functions/assets/user-variables.sh
fi

# Temp variables
if [ -e functions/assets/temp-variables.sh ] ; then
  source functions/assets/temp-variables.sh
fi

# Default paths
if [ -e functions/assets/default-paths.sh ] ; then
  source functions/assets/default-paths.sh
fi

# Custom paths
if [ -e functions/assets/custom-paths.sh ] ; then
  source functions/assets/custom-paths.sh
fi
