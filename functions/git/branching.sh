# THIS IS A WORK IN PROGRESS

# Script for ZSH

# Test files (docs test files)
test_branching() {
  echo 'functions/git/branching.sh ok'
}

# ---------------------------------------------------------------- #

# Create new branch from updated default branch

branch() {
  if [[ $1 ]]; then
    go_$1
  fi
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  echo "The default branch is `tput bold``tput setaf 2`$DEF_BRANCH`tput sgr0`."
  echo `tput setaf 3`"Enter a name for your new branch`tput sgr0` (allowed only: numbers, lowercase letters, dash, underscore):"
  read BRANCH_NAME
  echo `tput bold` "$ git checkout $DEF_BRANCH && git pull origin $DEF_BRANCH" `tput sgr0` ; git checkout $DEF_BRANCH && git pull origin $DEF_BRANCH
  echo `tput bold` "$ git checkout -b $BRANCH_NAME" `tput sgr0` ; git checkout -b $BRANCH_NAME
}

# Commit all and push to current repo and branch
commit() {
  if [[ $1 && $1 != "-p" ]]; then
    go_$1
  fi
  CURRENT_REPO=$(pwd)
  echo `tput setaf 5`"Pushing changes to: `tput sgr0 ; tput bold`$CURRENT_REPO"`tput sgr0`
  CURRENT_BRANCH=$(git branch --show-current)
  echo `tput setaf 5`"Staging changes to the branch: `tput sgr0 ; tput bold`$CURRENT_BRANCH"`tput sgr0`
  git add .
  echo `tput setaf 3`"Enter a commit message:"`tput sgr0`
  read COMMIT_MSG
  git commit -m "$COMMIT_MSG"
  if [[ $1 == "-p" || $2 == "-p" ]]; then
    git push origin $CURRENT_BRANCH
  fi
}

# help:

# `tput bold`- Changing files:`tput sgr0`
#   $ docs branch                                               # Creates a new branch in the repo currently open in the terminal
#   $ docs branch [repo]                                        # Creates a new branch in the [repo]
#                                                                 Repos: [gitlab], [omnibus], [runner], [charts], [docs], [shell]
#   $ docs commit                                               # Add and commit to the repo currently open in the terminal
#   $ docs commit -p                                            # Add, commit, and push to the repo currently open in the terminal
#   $ docs commit [repo]                                        # Add and commit to the [repo]
#   $ docs commit [repo] -p                                     # Add, commit, and push to the [repo]
#                                                                 Repos: [gitlab], [omnibus], [runner], [charts], [docs], [shell]
