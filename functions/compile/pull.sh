# Test file (docs test files)
test_pull() {
  echo 'compile/pull.sh ok'
}

# ---------------------------------------------------------------- #

# Pull all repos
# Usage: docs pull (pull and housekeep all repos); docs pull [repo] [repo] ... [repo]
# Examples: docs pull gitlab; docs pull gitlab runner; docs pull omnibus gitlab shell; docs pull docs
pull() {
  ssh-gitlab
  if [[ $@ ]] ; then
    for arg in "$@" ; do
      go_$arg
      if [[ $? != 0 ]]; then
        echo "Possible variations: \n $ docs pull \n $ docs pull [repo] [repo] ... [repo]"
        echo "   Options [repo]: gitlab, runner, omnibus, charts, docs, shell"
        return
      else
        tput setaf 5 ; echo "Pulling $arg" ; tput sgr0
        git_pull_check
      fi
    done
  else
    update -a
  fi
  cd $DSHELL
}

housekeep(){
  update -h -a
}

# Usage: docs reset [repo] [repo]
reset() {
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  echo "The default branch is `tput bold``tput setaf 2`$DEF_BRANCH`tput sgr0`."
  if [[ $@ ]] ; then
    for arg in "$@" ; do
      go_$arg
      if [[ $? != 0 ]]; then
        error_sound
        echo `tput setaf 1`"Repo `tput bold`'$arg'`tput sgr0``tput setaf 1` not found."`tput sgr0`
        echo "Run `tput bold`$ docs reset [repo] [repo] ... [repo]"`tput sgr0`
        if [[ $DGDK == '0' ]]; then
          echo "Available repos: gitlab, runner, omnibus, charts, docs, shell, gdk"
        else
          echo "Available repos: gitlab, runner, omnibus, charts, docs, shell"
        fi
        return
      else
        ok_sound ; tput setaf 3
        read "REPLY?`tput setaf 3`Are you sure you want to reset`tput bold` '$arg'`tput sgr0``tput setaf 3` against $DEF_BRANCH? All your changes `tput bold`will be lost`tput sgr0``tput setaf 3`. Continue (y/n)?"
        tput sgr0
        if [[ $REPLY =~ ^[Yy]$ ]] ; then
          if git diff-index --quiet HEAD --; then
            # There are no unstaged changes
            tput setaf 5 ; echo "Resetting $arg..." ; tput sgr0
            git_status
            echo `tput bold`"$ git reset --hard origin/$DEF_BRANCH" `tput sgr0`; git reset --hard origin/$DEF_BRANCH
          else
            ok_sound
            read "REPLY?There are unstaged changes. Stash them before resetting? (y/n)?"
            tput sgr0
            if [[ $REPLY =~ ^[Yy]$ ]] ; then
              echo `tput bold` "$ git stash" `tput sgr0`; git stash
              echo `tput bold`"$ git reset --hard origin/$DEF_BRANCH" `tput sgr0`; git reset --hard origin/$DEF_BRANCH
            else
              echo "Ok. Aborting."
            fi
          fi
        else
          echo "Ok. Aborting."
        fi
      fi
    done
  else
    error_sound
    echo "`tput setaf 1`Failed.`tput sgr0` You need to specify the repos you wish to reset:"
    echo "Run `tput bold`$ docs reset [repo] [repo] ...`tput sgr0`"
    if [[ $DGDK == '0' ]]; then
      echo "Available repos: gitlab, runner, omnibus, charts, docs, shell, gdk"
    else
      echo "Available repos: gitlab, runner, omnibus, charts, docs, shell"
    fi
  fi
  cd $DSHELL
}
