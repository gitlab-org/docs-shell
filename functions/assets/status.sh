test_status() {
  echo 'functions/assets/status.sh ok'
}

# Status codes

status_gl_com(){
  status_code=$(curl -s -o /dev/null -I -w %{http_code} https://gitlab.com/)
  if ping -c 1 -q gitlab.com >&/dev/null; then
    # GitLab.com is up and running.
  else
    echo "GitLab.com is not reachable. HTTP status: $status_code"
  fi
}
