## Navigation

test_navigation() {
  echo 'functions/assets/navigation.sh ok'
}

go_gl() {
  cd $GL
  tput sgr0; echo "I'm in $(pwd)"
}

go_gitlab() {
  cd $GITLAB
  tput sgr0; echo "I'm in $(pwd)"
}

go_omnibus() {
  cd $OMN
  tput sgr0; echo "I'm in $(pwd)"
}

go_runner() {
  cd $RUN
  tput sgr0; echo "I'm in $(pwd)"
}

go_charts() {
  cd $CHAR
  tput sgr0; echo "I'm in $(pwd)"
}

go_docs(){
  cd $DOC
  tput sgr0; echo "I'm in $(pwd)"
}

go_shell(){
  cd $DSHELL
  tput sgr0; echo "I'm in $(pwd)"
}

go_gdk(){
  if [[ $GDK_SEP ]]; then
    cd $GDK_SEP
    tput sgr0; echo "I'm in $(pwd)"
  else
    echo "GDK is not set."
  fi
}

go_gl_gdk(){
  if [[ $GDK_SEP_GL ]]; then
    cd $GDK_SEP_GL
    tput sgr0; echo "I'm in $(pwd)"
  else
    echo "GDK is not set."
  fi
}
