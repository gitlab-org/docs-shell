## Git

test_git() {
  echo 'functions/assets/git.sh ok'
}

git_status() {
  echo `tput bold` "$ git status" `tput sgr0`; git status
}

git_pull() {
  echo `tput bold` "$ git pull" `tput sgr0`; git pull
}

git_gc() {
  echo `tput bold` "$ git gc" `tput sgr0`; git gc
}

git_prune() {
  echo `tput bold` "$ git prune" `tput sgr0`; git prune
}

git_gc_prune(){
  echo `tput bold` "$ git gc --prune=30.minutes.ago" `tput sgr0`; git gc --prune=30.minutes.ago
}

git_master() {
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  echo `tput bold` "$ git checkout $DEF_BRANCH" `tput sgr0`; git checkout $DEF_BRANCH
}

git_stash() {
  echo `tput bold` "$ git stash" `tput sgr0`; git stash
}

git_config_rebase() {
  echo `tput bold` "$ git config pull.rebase false" `tput sgr0` ; git config pull.rebase false
}

git_fetch_origin(){
  echo `tput bold` "$ git fetch origin" `tput sgr0`; git fetch origin
}

## Git combinations

git_update_quick(){
  git_status; git_pull_check
}

git_update_house() {
  git_status; git_master; git_pull_check; git_gc_prune
}

git_checkout_master(){
  git_status; git_master;
}

git_stash_checkout_master(){
  git_status; git_stash; git_master;
}

# Check if there are changes before pulling
git_pull_check() {
  # Check what's the default branch (necessary for the master => main renaming)
  DEF_BRANCH=$(git remote show origin | grep "HEAD branch" | cut -d ":" -f 2 | tr -d ' ')
  echo "The default branch is `tput bold``tput setaf 2`$DEF_BRANCH`tput sgr0`."
  # Refresh git diff
  git update-index --refresh >/dev/null
  if (git diff-files --quiet --ignore-submodules --); then
      # There are no unstaged changes
      if (git branch --show-current | grep "^$DEF_BRANCH$" >/dev/null ); then
        # We're on the default branch
        echo `tput bold` "$ git fetch origin && git pull --ff-only origin $DEF_BRANCH" `tput sgr0`
        git fetch origin && git pull --ff-only origin $DEF_BRANCH
      else
        echo `tput bold` "$ git checkout $DEF_BRANCH" `tput sgr0`; git checkout $DEF_BRANCH
        echo `tput bold` "$ git fetch origin && git pull --ff-only origin $DEF_BRANCH" `tput sgr0`
        git fetch origin && git pull --ff-only origin $DEF_BRANCH
      fi
  else
    tput setaf 3 ; echo "You have unstaged changes."
    ok_sound ; read "REPLY?Would you like to stash your changes and pull $DEF_BRANCH (y/n)?`tput sgr0`
    "
      if [[ $REPLY =~ ^[Yy]$ ]] ; then
      echo `tput bold` "$ git stash" `tput sgr0`; git stash
      echo `tput bold` "$ git checkout $DEF_BRANCH" `tput sgr0`; git checkout $DEF_BRANCH
      echo `tput bold` "$ git fetch origin && git pull --ff-only origin $DEF_BRANCH" `tput sgr0`
      git fetch origin && git pull --ff-only origin $DEF_BRANCH
    else
      echo "Ok, repo will stay as-is."
    fi
  fi
}
