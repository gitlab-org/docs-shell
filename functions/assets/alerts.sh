## Alerts

test_alerts() {
  echo 'functions/assets/alerts.sh ok'
}

success() {
  tput bold
  tput setaf 2; printf "Done! `tput sgr0` (${NOW}) \n"
  tput sgr0
}

ifsuccess() {
  if [[ $? == 0 ]] ; then
    tput bold
    tput setaf 2; printf "Success `tput sgr0` (${NOW}) \n"
    tput sgr0
  else
    tput bold
    tput setaf 1; printf "Failed `tput sgr0` (${NOW}) \n"
    tput sgr0
  fi
}

bel(){
  tput bel
}

error_sound() {
  if [[ "$OSTYPE" == "darwin"* ]]; then
    afplay /System/Library/Sounds/Basso.aiff
  fi
}

ok_sound() {
  if [[ "$OSTYPE" == "darwin"* ]]; then
    afplay /System/Library/Sounds/Pop.aiff
  fi
}

invalid_cmd(){
  error_sound
  tput setaf 1 ; echo "Invalid command." ; tput sgr0
}

yay(){
  tput bold
  tput setaf 2; printf "${UNICORN} Yay! All done! `tput sgr0` (${NOW}) \n"
  bel
}
