# ------------------------------------------ #

# Docs maintenance tasks

maintenance (){
  go_gitlab
  echo "`tput setaf 5`Starting Docs Maintenance Tasks...`tput sgr0`"
  curr_branch=$(git branch --show-current)
  echo "Your current branch is `tput bold`$curr_branch`tput sgr0`."
  tput setaf 3
  ok_sound ; read "REPLY?Would you like to checkout a new up-to-date branch? (y/n)?"
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    echo "Enter the name to your new branch:"
    tput sgr0
    read NEW_BRANCH_MAINT_USER
    export NEW_BRANCH_MAINT=$NEW_BRANCH_MAINT_USER
    echo "`tput setaf 5`Fetching GitLab...`tput sgr0`"
    git_pull_check
    echo "`tput setaf 5`Checking out a new branch...`tput sgr0`"
    echo `tput bold`"$ git checkout -b $NEW_BRANCH_MAINT"`tput sgr0`
    git checkout -b $NEW_BRANCH_MAINT
    png-compress-images
    update_redirects
  else
    tput sgr0
    echo "Ok. Make sure your branch is up-to-date."
    tput setaf 3
    ok_sound ; read "REPLY?Would you like to abort Docs Maintenance Tasks? (y/n)?"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      tput sgr0
      echo "OK. Aborting."
    else
      png-compress-images
      update_redirects
    fi
  fi
}

# ------------------------------------------ #

# Compress images

png-compress-images(){
  echo `tput setaf 5`"Bundling GitLab..." `tput sgr0`
  echo `tput bold`"$ bundle install"`tput sgr0`
  bundle install
  png-install
  png-lint
  png-compress
}

png-install (){
  echo `tput setaf 5`"Checking pngquant's dependencies..." `tput sgr0`
  if brew ls --versions libpng > /dev/null ; then
    echo "`tput bold`libpng`tput sgr0` is installed."
  else
    echo "`tput bold`libpng`tput sgr0` is not installed."
    echo `tput setaf 5`"Installing `tput bold`libpng`tput sgr0`..." `tput sgr0`
    echo `tput bold`"$ brew install libpng"`tput sgr0`
    brew install libpng
  fi
  if brew ls --versions pngquant > /dev/null ; then
    echo "`tput bold`pngquant`tput sgr0` is installed."
  else
    echo "`tput bold`pngquant`tput sgr0` is not installed."
    echo `tput setaf 5`"Installing `tput bold`pngquant`tput sgr0`..."`tput sgr0`
    echo `tput bold`"$ brew install pngquant"`tput sgr0`
    brew install pngquant
  fi
}

png-lint (){
  echo `tput setaf 5`"Searching for uncompressed images..." `tput sgr0`
  echo `tput bold`"$ bundle exec rake pngquant:lint"`tput sgr0`
  bundle exec rake pngquant:lint > $DSHELL/functions/assets/uncompressed.txt
  total_uncompressed=$(grep -c "" $DSHELL/functions/assets/uncompressed.txt)
  if [[ $total_uncompressed > 0 ]] ; then
    tput setaf 2
    echo "Number of uncompressed images: `tput sgr0``tput bold`$total_uncompressed."
    tput sgr0
  else
    echo "There are no uncompressed images."
  fi
  rm $DSHELL/functions/assets/uncompressed.txt
}

png-compress (){
  echo `tput setaf 5`"Compressing images..." `tput sgr0`
  echo `tput bold`"$ bundle exec rake pngquant:compress"`tput sgr0`
  bundle exec rake pngquant:compress
}

# ------------------------------------------ #

# Update redirects

update_redirects(){
  search_redirects
  delete_redirects
  delete_redirect_temp_files
}

search_redirects (){
  # Search the files for redirect entries and add them to a temp file: redirects.txt
  go_gitlab
  tput setaf 5
  echo "Searching for redirected files (this may take a while)..."
  tput sgr0
  grep -ri "This redirect file can be deleted after" . > $DSHELL/functions/assets/redirects.txt
  total_redirects=$(grep -c "" $DSHELL/functions/assets/redirects.txt)
  tput setaf 2
  echo "Number of existing redirects: `tput sgr0``tput bold`$total_redirects`tput sgr0`."
  tput sgr0
  if [[ $total_redirects > 0 ]] ; then
    search_date
  else
    echo "There are no redirects."
  fi
}

delete_redirects() {
  to_remove="$DSHELL/functions/assets/removable-files.txt"
  total_files_to_remove=$(grep -c "" $DSHELL/functions/assets/removable-files.txt)
  if [[ $total_files_to_remove > 0 ]] ; then
    tput setaf 3
    echo "The following files will be deleted:"
    tput sgr0
    cat $to_remove
    tput setaf 3
    read "REPLY?Continue (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      cd $GITLAB
      rm $( cat $to_remove )
      tput setaf 2
      echo "Files removed."
      tput sgr0
    else
      echo "Aborted."
    fi
    tput bold
    echo "$ git status"
    tput sgr0
    git status
  else
    echo "There are no files to be removed."
  fi
}

delete_redirect_temp_files() {
  rm $DSHELL/functions/assets/removable-files.txt
  rm $DSHELL/functions/assets/redirects.txt
}

# ------------------------------------------ #

# Search for files within the dates
# Initial date = today
# End date = 40 days ago

search_date (){
  # IMPORTANT: Only works if you run the script AFTER the 20th of each month. Else, use the manual script:

  #   # Search the redirects file for dates 2021-0[6-7]-[0-2][0-9]:
  #   # Year: 2021
  #   # Months: 6 and 7
  #   # Days: 00 to 29 (today is 2021-07-29)
  #   # TO-DO: Read the date dynamically.
  # # Add only path to files to another temp file: removable-files.txt
  # tput setaf 5
  # echo "Searching for removable files..."
  # tput sgr0
  # grep -E '2021-0[6-7]-[0-2][0-9]' $DSHELL/functions/assets/redirects.txt | grep -ri "This redirect file can be deleted after" | cut -d ':' -f 2,4 > $DSHELL/functions/assets/removable-files.txt
  # removables=$(grep -c "" $DSHELL/functions/assets/removable-files.txt)
  # tput setaf 2
  # echo "Number of removable files: $removables."
  # tput sgr0

  TODAY=$(date +%Y-%m-%d)
  END_DATE=$(date -v-1m -v-10d +%Y-%m-%d)
  # Example: Today is 2021-08-24. Search files from 2021-07-14 until 2021-08-24 (1 month and 10 days ago).

  tput setaf 5
  echo "Searching for removable files between `tput sgr0``tput bold`$TODAY`tput sgr0` and `tput bold`$END_DATE`tput sgr0`..."
  tput sgr0

  # -------- Year --------
  INIT_DATE_YEAR=$(date +%Y)
  IDY=$(echo $INIT_DATE_YEAR | cut -c4)
  # Example: 202[1]

  END_DATE_YEAR=$(date -v-1m -v-10d +%Y)
  EDY=$(echo $END_DATE_YEAR | cut -c4)
  # Example: 202[1]

  # -------- Month --------
  INIT_DATE_MONTH=$(date +%m)
  # Example: 07
  IDM1=$(echo $INIT_DATE_MONTH | cut -c1)
  # Example: [0]7
  IDM2=$(echo $INIT_DATE_MONTH | cut -c2)
  # Example: 0[7]

  END_DATE_MONTH=$(date -v-1m -v-10d +%m)
  # Example: 08
  EDM1=$(echo $END_DATE_MONTH | cut -c1)
  # Example: [0]8
  EDM2=$(echo $END_DATE_MONTH | cut -c2)
  # Example: 0[8]

  # -------- Day --------
  INIT_DATE_DAY=$(date +%d)
  # Example: 14
  IDD1=$(echo $INIT_DATE_DAY | cut -c1)
  # Example: [1]4
  IDD2=$(echo $INIT_DATE_DAY | cut -c2)
  # Example: 1[4]

  END_DATE_DAY=$(date -v-1m -v-10d +%d)
  # Example: 24

  EDD1=$(echo $END_DATE_DAY | cut -c1)
  # Example: [2]4
  EDD2=$(echo $END_DATE_DAY | cut -c2)
  # Example: 2[4]

  DATES_FOUND=$(grep -E "202[$IDY-$EDY]-[$EDM1-$IDM1][$EDM2-$IDM2]-(?:0[1-9]|1[0-9]|2[0-$EDD2])" $DSHELL/functions/assets/redirects.txt | grep -ri "This redirect file can be deleted after" | cut -d '<' -f 3 | cut -d '>' -f 1 | sort| uniq)

  grep -E "202[$IDY-$EDY]-[$EDM1-$IDM1][$EDM2-$IDM2]-(?:0[1-9]|1[0-9]|2[0-$EDD2])" $DSHELL/functions/assets/redirects.txt | grep -ri "This redirect file can be deleted after" | cut -d ':' -f 2,4 > $DSHELL/functions/assets/removable-files.txt
  removables=$(grep -c "" $DSHELL/functions/assets/removable-files.txt)
  if [[ $removables > 0 ]] ; then
    tput setaf 2
    echo "Number of removable files: `tput sgr0``tput bold`$removables`tput sgr0`."
    tput setaf 3
    echo "Matching dates: \n`tput sgr0`$DATES_FOUND"
  fi
}
