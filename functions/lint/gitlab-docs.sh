# Test files (docs test files)
test_lint_gitlab-docs() {
  echo 'functions/lint/gitlab-docs.sh ok'
}

# ---------------------------------------------------------------- #

# Lint GitLab Docs #

docs_lint(){
  go_docs
  echo `tput setaf 5`"Linting Nanoc..." `tput sgr0`
  docs_lint_links
  docs_lint_anchors
  docs_lint_elinks
}

docs_lint_anchors() {
  go_docs
  echo `tput setaf 5`"Linting for broken anchors..." `tput sgr0`
  echo `tput bold` "$ bundle exec nanoc check internal_anchors" `tput sgr0`; bundle exec nanoc check internal_anchors
  ifsuccess
}

docs_lint_links() {
  go_docs
  echo `tput setaf 5`"Linting for broken links..." `tput sgr0`
  echo `tput bold` "$ bundle exec nanoc check internal_links" `tput sgr0`; bundle exec nanoc check internal_links
  ifsuccess
}

docs_lint_elinks() {
  go_docs
  echo `tput setaf 5`"Linting for broken external links..." `tput sgr0`
  echo `tput bold` "$ bundle exec nanoc check external_links" `tput sgr0`; bundle exec nanoc check external_links
  ifsuccess
}

docs_lint_while_live(){
  echo `tput setaf 3`"Running docs live preview on the background..." `tput sgr0`
  docs_live & docs_lint
  echo `tput setaf 3`"Refresh a page in the browser to bring docs live to the foreground." `tput sgr0`
}

docs_all() {
  go_docs
  docs_compile
  docs_lint_while_live
}

docs_recompile_lint(){
  go_docs
  docs_recompile
  docs_lint
}

docs_recompile_lint_live(){
  go_docs
  docs_recompile
  docs_lint_while_live
}

# ---------------------------------------------------------------- #

## GitLab Docs development linters ##

docs_dev(){
  go_docs
  yarn install --cache-folder .yarn-cache
  docs_dev_yarn
  docs_dev_jest
  docs_dev_scss
  docs_dev_yaml
  docs_dev_rspecs
  docs_dev_ci
}

docs_dev_yarn(){
  go_docs
  # JS tests
  echo `tput setaf 5`"Running JavaScript tests..." `tput sgr0`
  echo `tput bold` "$ yarn test" `tput sgr0`; yarn test
  ifsuccess
}

docs_dev_jest(){
  go_docs
  # Lint JS
  echo `tput setaf 5`"Running JavaScript linters..." `tput sgr0`
  echo `tput bold` "$ yarn eslint" `tput sgr0`; yarn eslint
  ifsuccess
  echo `tput bold` "$ yarn prettier" `tput sgr0`; yarn prettier
  ifsuccess
}

docs_dev_scss(){
  go_docs
  # Lint SCSS
  echo `tput setaf 5`"Running SCSS linter..." `tput sgr0`
  echo `tput bold` "$ bundle exec scss-lint" `tput sgr0`; bundle exec scss-lint
  ifsuccess
}

docs_dev_yaml() {
  go_docs
  # Yaml lint
  echo `tput setaf 5`"Running YAML linter..." `tput sgr0`
  echo `tput bold` "$ yamllint .gitlab-ci.yml content/_data" `tput sgr0`; yamllint .gitlab-ci.yml content/_data
  ifsuccess
}

docs_dev_ci() {
  # JS tests
  go_docs
  echo `tput setaf 5`"Running Yaml-lint for .gitlab-ci.yml..." `tput sgr0`
  echo `tput bold` "$ yamllint .gitlab-ci.yml content/_data" `tput sgr0`; yamllint .gitlab-ci.yml content/_data
  ifsuccess
}
