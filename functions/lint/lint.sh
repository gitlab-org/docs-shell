
# Usage:
# - docs lint # lint content and links
# - docs lint --content, docs lint -c # lints all content (markdownlint and vale)
# - docs lint -c gitlab; docs lint -c omnibus; docs lint -c runner; docs lint -c charts # lints content per repo
# - docs lint -c gitlab vale; docs lint -c gitlab markdown # lints content per repo per linter
# - docs lint --nanoc, docs lint -n # lints nanoc links
# - docs lint -n anchors, - docs lint -n links, - docs lint -n elinks
lint() {
  if [[ $@ ]]; then
    for arg in "$@" ; do
      case $arg in
        --content | -c)
          if [[ $3 ]] ; then
            REP="$2"
            LIN="$3"
            lint_${LIN}_${REP}
            shift 2
          else
            if [[ $2 ]] ; then
              REP="$2"
              lint_markdown_$REP
              lint_vale_$REP
              if [[ $2 == 'gitlab' ]]; then
                lint_views_gitlab
              fi
              shift
            else
              lint_all_content
            fi
          fi
        shift
        ;;
        --nanoc | -n)
          if [[ $2 ]] ; then
            DLIN="$2"
            docs_lint_${DLIN}
            shift
          else
            docs_lint
            shift
          fi
        shift
        ;;
        --dev | -d)
          if [[ $2 ]] ; then
            DLIN="$2"
            docs_dev_${DLIN}
            shift
          else
            docs_dev
            shift
          fi
        ;;
        # **)
        #   echo " `tput setaf 1` Whoops, command invalid. `tput sgr0`"
        #   --help
        # ;;
      esac
    done
  else
    lint_all_content
    docs_lint
  fi
}
